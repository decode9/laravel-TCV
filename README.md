# laravel-TCV
TCV trading API for Laravel

based on kraken laravel api from adman9000

## Install

#### Install via Composer

```
composer require decode9/laravel-TCV
```

Add the following lines to your `config/app.php`

```php
'providers' => [
        ...
        decode9\TCV\TCVServiceProvider::class,
        ...
    ],


 'aliases' => [
        ...
        'TCV' => decode9\TCV\TCVAPIFacade::class,
    ],
```

## Version

0.1.2!

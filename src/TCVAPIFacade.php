<?php

namespace decode9\TCV;

use Illuminate\Support\Facades\Facade;

class TCVAPIFacade extends Facade {

	protected static function getFacadeAccessor() {
		return 'TCV';
	}
}

<?php

namespace decode9\TCV;

/**
 * @author  decode9
 */
use Illuminate\Support\ServiceProvider;

class TCVServiceProvider extends ServiceProvider {

	public function boot()
	{
		$this->publishes([
			__DIR__.'/config/TCV.php' => config_path('TCV.php')
		]);
	} // boot

	public function register()
	{
		$this->mergeConfigFrom(__DIR__.'/config/TCV.php', 'TCV');
		$this->app->bind('TCV', function() {
			return new TCVAPI(config('TCV'));
		});



	} // register
}

<?php
namespace decode9\TCV;

class TCVAPI
{
    protected $key; // API key
    protected $url; // API base URL
    protected $version; // API version
    protected $curl; // curl handle
    /**
     * Constructor for TCV-API
     *
     * @param string $key API key
     * @param string $secret API secret
     * @param string $url base URL for TCV API
     * @param string $version API version
     * @param bool $sslverify enable/disable SSL peer verification.  disable if using beta test
     */
    public function __construct($config = false, $url = 'https://tcvex.tcv.com.ve:443/api', $version = 'v2', $sslverify = true)
    {
        if ($config) {
            $this->key = $config['TCV_JWT'];
        }

        $this->url = $url;
        $this->version = $version;
        $this->curl = curl_init();
        curl_setopt_array($this->curl, array(
            CURLOPT_SSL_VERIFYPEER => $sslverify,
            CURLOPT_SSL_VERIFYHOST => 2,
            CURLOPT_USERAGENT => 'TCV PHP API Agent',
            CURLOPT_POST => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 20,
            CURLOPT_TIMEOUT => 300)
        );
    }

    public function __destruct()
    {
        curl_close($this->curl);
    }

    public function setAPI($key)
    {
        $this->key = $key;
    }

    /**
     * ---------- PUBLIC FUNCTIONS ----------
     * getDepth
     * getMarkets
     * getOrderBook
     * getTrades
     * getTickers
     * getFee
     *
     **/
    /**
     * Get ticker
     *
     * @param asset pair code
     * @return asset pair ticker info
     */
    public function getDepth($code, $limit=300)
    {
        return $this->queryPublic('depth', [
            'market' => $code,
            'limit' => $limit
        ]);
    }

    /**
     * Get markets pairs
     *
     * @return array of pair names and their info
     */
    public function getMarkets()
    {
        return $this->queryPublic('markets');
    }

    /**
     * Get pair order book
     * @param code pair id code
     * @return array of order book
     */
    public function getOrderBook($code)
    {
        return $this->queryPublic('order_book', [
            'market' => $code,
        ]);
    }

    /**
     * Get pair Trades
     * @param code id market asset
     * @param int limit number
     * @param string order by
     * @param timestamp timestamp
     * @param from exchange code from
     * @param to exchange code to
     * @return array of all trades
     */

    public function getTrades($code, $limit, $order_by = 'desc', $timestamp = false, $from = false, $to = false)
    {
        return $this->queryPublic('trades', [
            'market' => $code,
            'limit' => $limit,
            'orde_by' => $order_by,
            'from' => $from,
            'to' => $to,
            'timestamp' => $timestamp,
        ]);
    }

    /**
     * Get Tickers
     * @param code id market asset
     * 
     * @return array of ticket market
     */

    public function getTickers($code='')
    {   
        $method = 'trades/' . $code;
        return $this->queryPublic($method);
    }

    /**
     * Get Fee
     * @param code method for get fee
     * 
     * @return array of fees by currency
     */

    public function getFee($code)
    {   
        $method = 'trades/' . $code;
        return $this->queryPublic($method);
    }

    //------ PRIVATE API CALLS ----------
    /*
     * getWallet
     * getBalance
     * getMyTrades
     * addOrder
     * trade (calls addOrder)
     * buyMarket
     * sellMarket
     * depositAddress
     * getOrder
     * deleteOrder
     * viewDeposits
     * withdrawInfo
     * withdrawFunds
     * viewWithdraw
     */
    /** Get Balances
     *
     * @param string currency
     * @param string label
     * 
     * @return array of asset wallet
     **/

    public function getWallet($currency, $label)
    {
        return $this->queryPrivate("deposit_address", ['currency' => $currency]);
    }

    /**
     * Get pair Private Trades
     * @param asset pair code
     * @param int limit number
     * @param string order by
     * @param timestamp timestamp
     * @param from exchange code from
     * @param to exchange code to
     * @return array of all trades
     */

    public function getMyTrades($code, $limit, $orde_by = 'desc', $timestamp = false, $from = false, $to = false)
    {
        return $this->queryPrivate('trades/my', [
            'market' => $code,
            'limit' => $limit,
            'orde_by' => $orde_by,
            'from' => $from,
            'to' => $to,
            'timestamp' => $timestamp,
        ]);
    }

    /**
     * Add Order
     *
     * 
     * @param   market = Pair of market
     * @param   price = price
     * @param   volume = order volume in lots
     **/

    public function addOrder($market, $side, $volume, $price)
    {
        return $this->queryPrivate('orders', array(
            
            'market' => $market,
            
            'side' => $side,
            
            'price' => $price,
            
            'volume' => $volume,
        ));
    }

    /**
     * Make a trade
     * calls addOrder
     **/
    public function trade($pair, $side, $volume, $price)
    {
        return $this->addOrder($pair, $side, $volume, $price);
    }

    /** Buy Market
     * Buy asset at the market price
     * @param asset pair
     * @param volume
     * @return order info
     **/
    public function buyMarket($pair, $volume, $price)
    {
        return $this->addOrder($pair, 'buy',  $volume, $price);
    }
    /** Sell Market
     * Sell asset at the market price
     * @param asset pair
     * @param volume
     * @return order info
     **/
    public function sellMarket($pair, $volume, $price)
    {
        return $this->addOrder($pair, 'sell', $volume, $price);
    }

    /** get Order
     * get order from TCV
     * @param id order id
     * @return order info
     **/
    public function getOrder($id)
    {
        return $this->queryPrivate('trade-order', array(
            'id' => $id,
        ));
    }

    /** delete Order
     * delete order from TCV
     * @param id order id
     * @return method info
     **/
    public function deleteOrder($id)
    {
        return $this->queryPrivate('order/delete', array(
            'id' => $id,
        ));
    }

    /**
     * Deposit Address
     * @param string $symbol   Asset symbol
     * @param string $label
     * @param string $method   Asset name?? If not set, find a method from the API
     * @return mixed
     **/
    public function depositAddress($symbol, $label)
    {
        return $this->getWallet($symbol, $label);
    }

    /**
     * View Deposits
     * @param string $symbol   Asset symbol
     * @return mixed
     **/

    //Sugerencia de comando para verificar el estado de los depositos
    public function viewDeposits($symbol, $state, $limit = 1000)
    {
        
        return $this->queryPrivate("deposits", ['currency' => $symbol, 'state' => $state, 'limit' => $limit]);

    }

   

    /**
     * Withdraw Info
     * @param string $symbol   Asset symbol
     * @return mixed
     **/

     //Sugerencia de comando para verificar información de retiro como coutas de retiro y fee de transacción 
     //La variable $key representa un nombre asociado a direcciones registradas dentro del exchange TCV.


    public function withdrawInfo($symbol, $key, $amount = 0)
    {

        return $this->queryPrivate("WithdrawInfo", ['currency' => $symbol, 'key' => $key, 'amount' => $amount]);

    }

    /**
     * Withdraw Funds
     * @param string $symbol   Asset symbol
     * @return mixed
     **/

     //Sugerencia de comando para retirar fondos 
     //La variable $key representa un nombre asociado a direcciones registradas dentro del exchange TCV.
    public function withdrawFunds($symbol, $key, $amount)
    {

        return $this->queryPrivate("Withdraw", ['currency' => $symbol, 'key' => $key, 'amount' => $amount]);

    }
    /**
     * View Withdraw
     * @param string $symbol   Asset symbol
     * @return mixed
     **/
    //Sugerencia de comando para revisar el estado de los retiros realizados
    public function viewWithdraw($symbol, $limit, $page = 1)
    {

        return $this->queryPrivate("withdraws", [
            'currency' => $symbol, 
            'page' => $page,
            'limit' => $limit
            ]);

    }

    /**
     * Query public methods
     *
     * @param string $method method name
     * @param array $request request parameters
     * @return array request result on success
     * @throws \Exception
     */
    private function queryPublic($method, array $request = array())
    {
        // build the POST data string
        $postdata = http_build_query($request, '', '&');

        // make request
        curl_setopt($this->curl, CURLOPT_URL, $this->url . '/' . $this->version . '/' . $method);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, array());
        $result = curl_exec($this->curl);
        if ($result === false) {
            throw new \Exception('CURL error: ' . curl_error($this->curl));
        }

        // decode results
        $result = json_decode($result, true);
        if (!is_array($result)) {
            throw new \Exception('JSON decode error');
        }

        return $result;
    }

    /**
     * Query private methods
     *
     * @param string $path method path
     * @param array $request request parameters
     * @return array request result on success
     * @throws TCVAPIException
     */
    public function queryPrivate($method, array $request = array())
    {

        // build the POST data string
        $postdata = http_build_query($request, '', '&');

        // set API key and sign the message
        $path = '/' . $this->version . '/' . $method;

        $headers = array(
            'Authorization: ' . $this->key,
        );

        // make request
        curl_setopt($this->curl, CURLOPT_URL, $this->url . $path);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($this->curl);

        if ($result === false) {
            throw new \Exception('CURL error: ' . curl_error($this->curl));
        }

        // decode results
        $result = json_decode($result, true);

        if (!is_array($result)) {
            throw new \Exception('JSON decode error');
        }

        return $result;
    }
}
